# README #

F. Gonzalez code challenge:

Given three boxes, implement the necessary logic and UI to allow the user to move the red box by clicking on the "left" button. Bear in mind that, in the future, more boxes could be added and that doing so should not affect the logic of the solution. If the red box is in the first position, clicking “Left” should move it to the last position. 



const divs = Array.from(document.querySelectorAll("div"));
let active = 0;
/**
*   Function Prev
*
*/
function prev() {
    active -=  1;
    if(active < 0) {
        active = divs.length -1;
    }
    document.querySelector(".active").classList.remove("active");
    divs[active].classList.add("active");
}
document.querySelector(".prev").addEventListener("click", prev);
divs[active].classList.add("active");
